using OpenHumanDesign.Visualization.Schema;

namespace OpenHumanDesign.Visualization;

class CenterShape
{
	public readonly BodygraphCenterType Type;
	public readonly CenterSchema Schema;
	public readonly Point Position;

	readonly Lazy<Point[]> _shape;
	public Point[] Shape => _shape.Value;

	readonly Lazy<LineSection[]> _innerEdges;
	public LineSection[] InnerEdges => _innerEdges.Value;
	
	public CenterShape(CenterSchema schema, Point position)
	{
		Schema = schema;
		Position = position;
		Type = schema.Type;

		_shape = new Lazy<Point[]>(GetShape(schema.Shape).Select(p => p + position).ToArray());
		_innerEdges = new Lazy<LineSection[]>(GetInnerEdges(schema.Radius).ToArray());
	}

	IEnumerable<Point> GetShape(ShapeType shape)
	{
		var halfWidth = Schema.Size.Width / 2;
		var halfHeight = Schema.Size.Height / 2;
		
		switch (shape)
		{
		case ShapeType.Rect:
			yield return new Point(-halfWidth, halfHeight);
			yield return new Point(halfWidth, halfHeight);
			yield return new Point(halfWidth, -halfHeight);
			yield return new Point(-halfWidth, -halfHeight);
			break;
		case ShapeType.Diamond:
			yield return new Point(0, halfHeight);
			yield return new Point(halfWidth, 0);
			yield return new Point(0, -halfHeight);
			yield return new Point(-halfWidth, 0);
			break;
		case ShapeType.TriangleUp:
			yield return new Point(0, halfHeight);
			yield return new Point(halfWidth, -halfHeight);
			yield return new Point(-halfWidth, -halfHeight);
			break;
		case ShapeType.TriangleDown:
			yield return new Point(-halfWidth, halfHeight);
			yield return new Point(halfWidth, halfHeight);
			yield return new Point(0, -halfHeight);
			break;
		case ShapeType.TriangleLeft:
			yield return new Point(halfWidth, halfHeight);
			yield return new Point(halfWidth, -halfHeight);
			yield return new Point(-halfWidth, 0);
			break;
		case ShapeType.TriangleRight:
			yield return new Point(-halfWidth, halfHeight);
			yield return new Point(halfWidth, 0);
			yield return new Point(-halfWidth, halfHeight);
			break;
		case ShapeType.Octagon:
			var quarterWidth = Schema.Size.Width / 4;
			var quarterHeight = Schema.Size.Width / 4;
			yield return new Point(-quarterWidth, halfHeight);
			yield return new Point(quarterWidth, halfHeight);
			yield return new Point(halfWidth, quarterHeight);
			yield return new Point(halfWidth, -quarterHeight);
			yield return new Point(quarterWidth, -halfHeight);
			yield return new Point(-quarterWidth, -halfHeight);
			yield return new Point(-halfWidth, -quarterHeight);
			yield return new Point(-halfWidth, quarterHeight);
			break;
		default:
			throw new ArgumentOutOfRangeException();
		}
	}

	IEnumerable<LineSection> GetInnerEdges(decimal radius)
	{
		var newPoints = new List<Point>();
		var last = Shape.Length - 1;
		for (var i = -1; i < last; ++i)
		{
			var v1 = Shape[i >= 0 ? i : last];
			var v2 = Shape[(i + 1) % Shape.Length];
			var v3 = Shape[(i + 2) % Shape.Length];

			var v21 = (v2 - v1).Norm();
			var d21 = new Point(-v21.Y, v21.X) * radius;
			var v1N1 = v1 + d21;
			var v2N1 = v2 + d21;

			var v23 = (v2 - v3).Norm();
			var d23 = new Point(v23.Y, -v23.X) * radius;
			var v2N2 = v2 + d23;
			var v3N2 = v3 + d23;

			var pt = VectorUtils.Intersect(v1N1, v2N1, v2N2, v3N2) ?? new Point(0, 0);
			newPoints.Add(pt);
		}

		for (var i = 0; i < newPoints.Count; ++i)
			yield return new LineSection(newPoints[i], newPoints[(i + 1) % newPoints.Count]);
	}

	public void Render(SeparatedStringBuilder builder)
	{
		
	}
}
