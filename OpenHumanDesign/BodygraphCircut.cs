namespace OpenHumanDesign;

public enum BodygraphCircut
{
	Integration,
	Knowing,
	Centering,
	Understanding,
	Sensing,
	Defence,
	Ego
}
