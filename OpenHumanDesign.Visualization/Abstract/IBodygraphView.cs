using OpenHumanDesign.Visualization.Centers;

namespace OpenHumanDesign.Visualization.Abstract;

public interface IBodygraphView : IBodygraphViewPart
{
	BodygraphCentersCollection<IBodygraphCenterView> Centers { get; }
}
