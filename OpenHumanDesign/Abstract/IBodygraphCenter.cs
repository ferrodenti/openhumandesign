namespace OpenHumanDesign.Abstract;

public interface IBodygraphCenter
{
	IBodygraphGate[] Gates { get; }
	BodygraphCenterType Type { get; }
}