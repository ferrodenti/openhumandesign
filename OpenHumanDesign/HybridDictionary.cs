using System.Collections;
using System.Collections.Specialized;

namespace OpenHumanDesign;

public class HybridDictionary<TKey, TValue> 
	: IDictionary<TKey, TValue>
	where TKey : notnull
{
	readonly HybridDictionary _inner = new();


	public HybridDictionary()
	{
	}

	public HybridDictionary(IEnumerable<KeyValuePair<TKey,TValue>> pairs)
	{
		foreach (var pair in pairs)
			Add(pair.Key, pair.Value);
	}

	public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		=> _inner.Cast<KeyValuePair<TKey, TValue>>().GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	public void Add(KeyValuePair<TKey, TValue> item)
		=> _inner.Add(item.Key, item.Value);

	public void Clear()
		=> _inner.Clear();

	public bool Contains(KeyValuePair<TKey, TValue> item)
		=> _inner.Contains(item.Key);

	public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		=> _inner.Cast<KeyValuePair<TKey, TValue>>().ToArray().CopyTo(array, arrayIndex);

	public bool Remove(KeyValuePair<TKey, TValue> item)
		=> Remove(item.Key);

	public int Count => _inner.Count;
	public bool IsReadOnly => _inner.IsReadOnly;
	
	public void Add(TKey key, TValue value)
		=> _inner.Add(key, value);

	public bool ContainsKey(TKey key)
		=> _inner.Contains(key);

	public bool Remove(TKey key)
	{
		if (_inner.Contains(key))
		{
			_inner.Remove(key);
			return true;
		}
		return false;
	}

	public bool TryGetValue(TKey key, out TValue value)
	{
		if (ContainsKey(key))
		{
			value = (TValue)_inner[key];
			return true;
		}

		value = default!;
		return false;
	}

	public TValue this[TKey key]
	{
		get => (TValue)_inner[key];
		set => _inner[key] = value;
	}

	public ICollection<TKey> Keys => _inner.Keys.Cast<TKey>().ToArray();
	public ICollection<TValue> Values => _inner.Keys.Cast<TValue>().ToArray();
}
