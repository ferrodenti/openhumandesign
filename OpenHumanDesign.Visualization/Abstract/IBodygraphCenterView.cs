﻿using OpenHumanDesign.Abstract;

namespace OpenHumanDesign.Visualization.Abstract;

public interface IBodygraphCenterView : IBodygraphViewPart, IBodygraphCenter
{
	LineSection[] InnerEdges { get; }
}