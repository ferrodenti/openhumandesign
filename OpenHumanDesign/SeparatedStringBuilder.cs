using System.Text;

namespace OpenHumanDesign;

public class SeparatedStringBuilder
{
	readonly StringBuilder _stringBuilder = new();

	public string Separator { get; }

	public SeparatedStringBuilder(string separator)
		=> Separator = separator;

	public void Append(object? obj)
	{
		if (obj == null)
			return;
		
		if (_stringBuilder.Length > 0)
			_stringBuilder.Append(Separator);

		_stringBuilder.Append(obj);
	}

	public override string ToString()
		=> _stringBuilder.ToString();

	public static implicit operator string(SeparatedStringBuilder bld)
		=> bld.ToString();
}