using OpenHumanDesign.Visualization.Abstract;

namespace OpenHumanDesign.Visualization;

public class ChannelView : IBodygraphChannelView
{
	public IBodygraphGateView Gate1 { get; }
	public IBodygraphGateView Gate2 { get; }

	Point? _midPoint;
	public Point MidPoint
	{
		get
		{
			if(!_midPoint.HasValue && Gate1.CenterPoint.HasValue && Gate2.CenterPoint.HasValue)
				_midPoint = (Gate1.CenterPoint + Gate2.CenterPoint) / 2;

			return _midPoint ?? new Point();
		}
		set => _midPoint = value;
	}

	public ChannelView(IBodygraphGateView gate1, IBodygraphGateView gate2)
	{
		Gate1 = gate1;
		Gate2 = gate2;
	}

	public void ResetMidPoint()
		=> _midPoint = null;
	
	public string RenderSvg()
	{
		if (!Gate1.CenterPoint.HasValue ||
		    !Gate2.CenterPoint.HasValue)
			return "";

		return $@"
<line x1='{Gate1.CenterPoint.Value.X}' y1='{Gate1.CenterPoint.Value.Y}' x2='{MidPoint.X}' y2='{MidPoint.Y}' class='channel-contour' />
<line x1='{Gate2.CenterPoint.Value.X}' y1='{Gate2.CenterPoint.Value.Y}' x2='{MidPoint.X}' y2='{MidPoint.Y}' class='channel-contour' />
<line x1='{Gate1.CenterPoint.Value.X}' y1='{Gate1.CenterPoint.Value.Y}' x2='{MidPoint.X}' y2='{MidPoint.Y}' class='channel' />
<line x1='{Gate2.CenterPoint.Value.X}' y1='{Gate2.CenterPoint.Value.Y}' x2='{MidPoint.X}' y2='{MidPoint.Y}' class='channel' />";
	}
}
