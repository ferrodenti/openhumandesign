﻿namespace OpenHumanDesign.Visualization.Abstract;

public interface IBodygraphViewPart
{
	string RenderSvg();
}