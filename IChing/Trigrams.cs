namespace IChing;

class Trigrams : NGramsSet<Trigrams, Trigram>
{
	public Trigrams() : base(3)
	{
	}
	
	protected override IEnumerable<Trigram> Init()
	{
		yield return new Trigram(this, 1, '☰', "111");
		yield return new Trigram(this, 2, '☱', "011");
		yield return new Trigram(this, 3, '☲', "101");
		yield return new Trigram(this, 4, '☳', "001");
		yield return new Trigram(this, 5, '☴', "110");
		yield return new Trigram(this, 6, '☵', "010");
		yield return new Trigram(this, 7, '☶', "100");
		yield return new Trigram(this, 8, '☷', "000");
	}
}
