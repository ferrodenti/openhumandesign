using System.Collections;

namespace IChing;

public class OneBasedArray<T> : IReadOnlyList<T> //TODO: Debug view
{
	readonly int _capacity = 64;
	List<T>? _addAccumulator;
	
	T[] ZeroBasedInner { get; set; }

	public OneBasedArray()
		=> ZeroBasedInner = Array.Empty<T>();

	public OneBasedArray(int capacity) : this()
		=> _capacity = capacity;

	public OneBasedArray(T[] zeroBasedEquivalent)
		=> ZeroBasedInner = zeroBasedEquivalent;

	public OneBasedArray(IEnumerable<T> zeroBasedEquivalent)
		=> ZeroBasedInner = zeroBasedEquivalent.ToArray();

	public IEnumerator<T> GetEnumerator()
		=> ZeroBasedInner.Cast<T>().GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	internal void Add(T item)
	{
		_addAccumulator ??= new List<T>(_capacity);
		_addAccumulator.Add(item);
		ZeroBasedInner = _addAccumulator.ToArray();
	}

	public int Count => ZeroBasedInner.Length;

	public T this[int index]
	{
		get
		{
			if (index < 1 || index > Count)
				throw new IndexOutOfRangeException($"Index was outside the bounds of the array [{(Count == 0? "empty": $"1...{Count}")}]");
			
			return ZeroBasedInner[index - 1];
		}
	}
}
