namespace IChing;

public interface INGram<out TThis> where  TThis : INGram<TThis>
{
	char Symbol { get; }
	int Number { get; }
	int BinaryValue { get; }
	OneBasedArray<bool> Lines { get; }
	TThis Partner { get; }
}
