using System.Collections;
using IChing;
using OpenHumanDesign.Abstract;

namespace OpenHumanDesign;

public class BodygraphChannelsCollection : IReadOnlyCollection<IBodygraphChannel>
{
	readonly HybridDictionary<(int, int), IBodygraphChannel> _inner;

	public BodygraphChannelsCollection(IEnumerable<IBodygraphChannel> channels)
		=> _inner = new HybridDictionary<(int, int), IBodygraphChannel>(channels.Select(ch => KeyValuePair.Create(GetKey(ch), ch)));
	
	IEnumerator<IBodygraphChannel> IEnumerable<IBodygraphChannel>.GetEnumerator()
		=> _inner.Values.Cast<IBodygraphChannel>().GetEnumerator();

	public IEnumerator<IBodygraphChannel> GetEnumerator()
		=> _inner.Values.GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	static (int, int) GetKey(Hexagram a, Hexagram b)
		=> a < b ? (a, b) : (b, a);

	static (int, int) GetKey(IBodygraphChannel channel)
		=> GetKey(channel.Gate1.Hexagram, channel.Gate2.Hexagram);

	public int Count => _inner.Count;
	
	public bool TryGetValue(Hexagram a, Hexagram b, out IBodygraphChannel? result)
		=> _inner.TryGetValue(GetKey(a,b), out result);
	
	public IEnumerable<IBodygraphChannel> this[Hexagram index]
	{
		get
		{
			foreach (var ((a, b), item) in _inner)
				if (a == index || b == index)
					yield return item;
		}
	}
	
	public IBodygraphChannel? this[Hexagram a, Hexagram b] => TryGetValue(a, b, out var result) ? result : default;
}
