namespace IChing;

[Serializable]
public readonly struct Trigram : INGram<Trigram>
{
	public char Symbol { get; }
	public int Number { get; }
	public int BinaryValue { get; }

	public OneBasedArray<bool> Lines { get; }

	readonly Lazy<Trigram> _partnerLazy;
	public Trigram Partner => _partnerLazy.Value;
	
	internal Trigram(Trigrams trigrams, int number, char symbol, string bits)
	{
		Number = number;
		Symbol = symbol;

		(Lines, BinaryValue, var partnerBits) = trigrams.InitNGram(bits);
		_partnerLazy = new Lazy<Trigram>(() => trigrams.GetByBinaryValue(partnerBits));
		trigrams.RegisterNGram(this);
	}
	

	public override string ToString()
		=> $"{Symbol}({Number})";

	public override int GetHashCode()
		=> 101 + Number;

	public override bool Equals(object obj)
		=> obj is Trigram h && h.Number == Number;

	public static Trigram Get(int number)
		=> Trigrams.Instance.GetByNumber(number);

	public static Trigram GetByBinaryValue(int value)
		=> Trigrams.Instance.GetByBinaryValue(value);
	
	public static implicit operator int(Trigram hexagram)
		=> hexagram.Number;

	public static implicit operator Trigram(int number)
		=> Trigrams.Instance.GetByNumber(number);
	
	public static Trigram Parse(string value)
		=> Trigrams.Instance.Parse(value);

	public static bool TryParse(string value, out Trigram result)
		=> Trigrams.Instance.TryParse(value, out result);

	public static Trigram? TryParse(string value)
		=> Trigrams.Instance.TryParse(value);
}
