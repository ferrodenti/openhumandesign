namespace OpenHumanDesign.Visualization;

public class Rect
{
	public readonly Point Center;
	public readonly Point TopLeft;
	public readonly Point TopRight;
	public readonly Point BottomLeft;
	public readonly Point BottomRight;
	public readonly decimal Width;
	public readonly decimal Height;
	public readonly decimal HalfWidth;
	public readonly decimal HalfHeight;

	public Rect(Point center, decimal width, decimal height)
	{
		Center = center;
		Width = width;
		Height = height;
		HalfWidth = Width / 2m;
		HalfHeight = Height / 2m;
		TopLeft = center - new Point(HalfWidth, HalfHeight);
		BottomRight = TopLeft + new Point(Width, Height);
		TopRight = new Point(BottomRight.X, TopLeft.Y);
		BottomLeft = new Point(TopLeft.X, BottomRight.Y);
	}
}