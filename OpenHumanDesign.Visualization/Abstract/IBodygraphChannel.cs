namespace OpenHumanDesign.Visualization.Abstract;

public interface IBodygraphChannelView : IBodygraphViewPart
{
	void ResetMidPoint();
}
