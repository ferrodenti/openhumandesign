using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;
using IChing;

namespace OpenHumanDesign.Visualization.Schema;

public class GateSchema
{
	[XmlIgnore]
	public Hexagram Hexagram { get; set; }

	[XmlAttribute("Hexagram")]
	public int HexagramNo
	{
		get => Hexagram;
		set => Hexagram = value;
	}
	
	[XmlAttribute]
	public int Edge { get; set; }
	
	[XmlAttribute]
	public decimal Position { get; set; }
	
	[XmlAttribute]
	public int XAsGate { get; set; }

	public bool ShouldSerializeXAsGate()
		=> XAsGate != 0;

	[XmlIgnore]
	public Point TextShift { get; set; }

	[XmlAttribute("TextShift")]
	public string TextShiftStr
	{
		get => TextShift.ToString();
		set => TextShift = Point.TryParse(value, out var p) ? p : new Point();
	}
	public bool ShouldSerializeTextShiftStr()
		=> TextShift.IsSome;

	public GateSchema()
	{
	}

	public GateSchema(Hexagram hexagram, int edgeNo, decimal position)
	{
		Hexagram = hexagram;
		Edge = edgeNo;
		Position = position;
	}
	public GateSchema(Hexagram hexagram, int edgeNo, Hexagram xAsGateNo)
	{
		Hexagram = hexagram;
		Edge = edgeNo;
		XAsGate = xAsGateNo;
	}

	[SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
	public override int GetHashCode()
	{
		var hash = new HashCode();
		hash.Add(Edge);
		hash.Add(Hexagram);
		hash.Add(Position);
		hash.Add(TextShift);
		hash.Add(XAsGate);
		return hash.ToHashCode();
	}
}
