using OpenHumanDesign.Abstract;

namespace OpenHumanDesign;

public class BodygraphChannel : IBodygraphChannel
{
	public IBodygraphGate Gate1 { get; }
	public IBodygraphGate Gate2 { get; }
	public BodygraphCircut Circut { get; }
	
	public BodygraphChannel(IBodygraphGate gate1, IBodygraphGate gate2, BodygraphCircut circut)
	{
		Gate1 = gate1;
		Gate2 = gate2;
		Circut = circut;
	}

}
