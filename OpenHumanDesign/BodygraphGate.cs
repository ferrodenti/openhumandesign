using IChing;
using OpenHumanDesign.Abstract;

namespace OpenHumanDesign;

public class BodygraphGate : IBodygraphGate
{
	public Hexagram Hexagram { get; }
	public BodygraphCenter Center { get; }
	
	public BodygraphGate(Hexagram hexagram, BodygraphCenter center)
	{
		Center = center;
		Hexagram = hexagram;
	}
}
