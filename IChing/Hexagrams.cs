using System.Diagnostics;

namespace IChing;

class Hexagrams : NGramsSet<Hexagrams, Hexagram>
{
	public Hexagrams() : base(6)
	{
	}

	protected override void AdditionalCheck(Hexagram hexagram)
	{
		var upper = hexagram.UpperTrigram;
		var lower = hexagram.LowerTrigram;
		Debug.Assert(hexagram.BinaryValue == ((lower.BinaryValue << 3) | upper.BinaryValue));
	}

	protected override IEnumerable<Hexagram> Init()
	{
		yield return new Hexagram(this, 1, '䷀', "111111");
		yield return new Hexagram(this, 2, '䷁', "000000");
		yield return new Hexagram(this, 3, '䷂', "010001");
		yield return new Hexagram(this, 4, '䷃', "100010");
		yield return new Hexagram(this, 5, '䷄', "010111");
		yield return new Hexagram(this, 6, '䷅', "111010");
		yield return new Hexagram(this, 7, '䷆', "000010");
		yield return new Hexagram(this, 8, '䷇', "010000");
		yield return new Hexagram(this, 9, '䷈', "110111");
		yield return new Hexagram(this, 10, '䷉', "111011");
		yield return new Hexagram(this, 11, '䷊', "000111");
		yield return new Hexagram(this, 12, '䷋', "111000");
		yield return new Hexagram(this, 13, '䷌', "111101");
		yield return new Hexagram(this, 14, '䷍', "101111");
		yield return new Hexagram(this, 15, '䷎', "000100");
		yield return new Hexagram(this, 16, '䷏', "001000");
		yield return new Hexagram(this, 17, '䷐', "011001");
		yield return new Hexagram(this, 18, '䷑', "100110");
		yield return new Hexagram(this, 19, '䷒', "000011");
		yield return new Hexagram(this, 20, '䷓', "110000");
		yield return new Hexagram(this, 21, '䷔', "101001");
		yield return new Hexagram(this, 22, '䷕', "100101");
		yield return new Hexagram(this, 23, '䷖', "100000");
		yield return new Hexagram(this, 24, '䷗', "000001");
		yield return new Hexagram(this, 25, '䷘', "111001");
		yield return new Hexagram(this, 26, '䷙', "100111");
		yield return new Hexagram(this, 27, '䷚', "100001");
		yield return new Hexagram(this, 28, '䷛', "011110");
		yield return new Hexagram(this, 29, '䷜', "010010");
		yield return new Hexagram(this, 30, '䷝', "101101");
		yield return new Hexagram(this, 31, '䷞', "011100");
		yield return new Hexagram(this, 32, '䷟', "001110");
		yield return new Hexagram(this, 33, '䷠', "111100");
		yield return new Hexagram(this, 34, '䷡', "001111");
		yield return new Hexagram(this, 35, '䷢', "101000");
		yield return new Hexagram(this, 36, '䷣', "000101");
		yield return new Hexagram(this, 37, '䷤', "110101");
		yield return new Hexagram(this, 38, '䷥', "101011");
		yield return new Hexagram(this, 39, '䷦', "010100");
		yield return new Hexagram(this, 40, '䷧', "001010");
		yield return new Hexagram(this, 41, '䷨', "100011");
		yield return new Hexagram(this, 42, '䷩', "110001");
		yield return new Hexagram(this, 43, '䷪', "011111");
		yield return new Hexagram(this, 44, '䷫', "111110");
		yield return new Hexagram(this, 45, '䷬', "011000");
		yield return new Hexagram(this, 46, '䷭', "000110");
		yield return new Hexagram(this, 47, '䷮', "011010");
		yield return new Hexagram(this, 48, '䷯', "010110");
		yield return new Hexagram(this, 49, '䷰', "011101");
		yield return new Hexagram(this, 50, '䷱', "101110");
		yield return new Hexagram(this, 51, '䷲', "001001");
		yield return new Hexagram(this, 52, '䷳', "100100");
		yield return new Hexagram(this, 53, '䷴', "110100");
		yield return new Hexagram(this, 54, '䷵', "001011");
		yield return new Hexagram(this, 55, '䷶', "001101");
		yield return new Hexagram(this, 56, '䷷', "101100");
		yield return new Hexagram(this, 57, '䷸', "110110");
		yield return new Hexagram(this, 58, '䷹', "011011");
		yield return new Hexagram(this, 59, '䷺', "110010");
		yield return new Hexagram(this, 60, '䷻', "010011");
		yield return new Hexagram(this, 61, '䷼', "110011");
		yield return new Hexagram(this, 62, '䷽', "001100");
		yield return new Hexagram(this, 63, '䷾', "010101");
		yield return new Hexagram(this, 64, '䷿', "101010");
	}
}
