using System.Diagnostics;

namespace IChing;

public abstract class NGramsSet<TThis, TNGram> 
	where TThis : NGramsSet<TThis, TNGram>, new()
	where TNGram : INGram<TNGram>
{
	public static readonly TThis Instance = new TThis().Prepare();
	
	public readonly int NumberOfLines;
	public readonly int NumberOfNGrams;
	public readonly int BinaryMask;

	public readonly string NGramName;

	OneBasedArray<TNGram>? _inner;
	readonly Dictionary<int, TNGram> _binaryValues = new();

	protected NGramsSet(int numberOfLines)
	{
		NumberOfLines = numberOfLines;
		NGramName = typeof(TNGram).Name.ToLower();

		NumberOfNGrams = 2 << (NumberOfLines - 1);
		BinaryMask = NumberOfNGrams - 1;
	}

	protected TThis Prepare()
	{
		var result = new OneBasedArray<TNGram>(Init());
		CheckNGrams(result);
		_inner = result;
		return (TThis)this;
	}

	protected abstract IEnumerable<TNGram> Init();

	public (OneBasedArray<bool> lines, byte binaryValue, int partnerBits) InitNGram(string bits)
	{
		Debug.Assert(bits.Length == NumberOfLines, $"Invalid bits value: \"{bits}\".length={bits.Length} (must be {NumberOfLines}!)");

		var lines = new OneBasedArray<bool>(bits.Select(ch => ch switch
		{
			'0' => false,
			'1' => true,
			_ => throw new Exception($"Invalid bits value: {bits}")
		}));

		var binaryValue = BooleansToByte(lines);
		var partnerBits = (~binaryValue) & BinaryMask;

		return (lines, binaryValue, partnerBits);
	}

	public void RegisterNGram(TNGram nGram)
		=> _binaryValues.Add(nGram.BinaryValue, nGram);

	byte BooleansToByte(IReadOnlyCollection<bool> source)
	{
		byte result = 0;
		var i = 0;

		Debug.Assert(source.Count >= NumberOfLines, $"Invalid source length: {source.Count}");

		foreach (var bit in source)
		{
			if (bit)
				result |= (byte)(1 << i);

			++i;
		}

		return result;
	}

	public TNGram GetByNumber(int number)
	{
		if (number < 1 || number > NumberOfNGrams)
			throw new ArgumentException($"A value must be between 1 and {NumberOfNGrams}", nameof(number));

		return _inner![number];
	}
	
	public TNGram GetByBinaryValue(int value)
		=> _binaryValues[value & BinaryMask];

	[Conditional("DEBUG")]
	void CheckNGrams(OneBasedArray<TNGram> result)
	{
		Debug.Assert(result.Count == NumberOfNGrams, $"Invalid {NGramName}s' count: {result.Count}");
		
		var i = 1;
		var symbols = new HashSet<char>();
		var lines = new HashSet<string>();
		var partners = new Dictionary<TNGram,TNGram>();
		
		foreach (var ngram in result)
		{
			Debug.Assert(ngram.Number == i++, $"Invalid {NGramName} number: {ngram}");
			Debug.Assert(symbols.Add(ngram.Symbol), $"Invalid {NGramName} symbol: {ngram}");

			var str = string.Concat(ngram.Lines.Select(l => l ? "1" : "0"));
			Debug.Assert(lines.Add(str), $"Invalid {NGramName} lines: {ngram}, {str}");

			var partner = ngram.Partner;
			var partner2 = partner.Partner;
			Debug.Assert(Equals(partner2, ngram), $"Partners missmatch: {ngram}.Partner={partner}, {partner}.Partner={partner2}");
			Debug.Assert(partners.TryAdd(partner, ngram), $"Partner already registred: {{hexagram}}.Partner={{partner}}, partners[{partner}]={partners[partner]}");

			AdditionalCheck(ngram);
		}
	}

	protected virtual void AdditionalCheck(TNGram nGram)
	{
	}

	public TNGram Parse(string value)
	{
		if (TryParse(value, out var result))
			return result;

		throw new ArgumentOutOfRangeException(nameof(value), value, "Invalid argument");
	}

	public TNGram? TryParse(string value)
		=> TryParse(value, out var result) ? result : default;
	
	public bool TryParse(string value, out TNGram result)
	{
		if(int.TryParse(value, out var number) && number >= 1 && number <= NumberOfNGrams)
		{
			result = GetByNumber(number);
			return true;
		}

		result = GetByNumber(1);
		return false;
	}
}
