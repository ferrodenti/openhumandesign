using OpenHumanDesign.Abstract;

namespace OpenHumanDesign.Visualization.Abstract;

public interface IBodygraphGateView : IBodygraphViewPart, IBodygraphGate
{
	Point? CenterPoint { get; }
	Point? TextShift { get; set; }

	IBodygraphGateView BindToCenterPosition(int edgeNo, decimal position);
	IBodygraphGateView BindToCenterX(int edgeNo, decimal x);
}
