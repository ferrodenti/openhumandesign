namespace OpenHumanDesign;

public enum BodygraphCenterType
{
	None,
	Throat,
	Head,
	Root,
	Ajna,
	Splenic,
	SolarPlexus,
	Heart,
	Sacral,
	G
}
