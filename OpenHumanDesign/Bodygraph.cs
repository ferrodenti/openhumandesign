using IChing;
using OpenHumanDesign.Abstract;

namespace OpenHumanDesign;

public class Bodygraph : IBodygraph
{
	public BodygraphGatesCollection Gates { get; }
	IReadOnlyList<IBodygraphGate> IBodygraph.Gates => Gates;
	
	public BodygraphChannelsCollection Channels { get; }
	IReadOnlyCollection<IBodygraphChannel> IBodygraph.Channels => Channels;
	
	public BodygraphCentersCollection<BodygraphCenter> Centers { get; }
	IReadOnlyCollection<IBodygraphCenter> IBodygraph.Centers => Centers;

	public string Name { get; set; }

	public Bodygraph()
	{
		Name = "Bodygraph";
		
		#region Centers
		
		var gatesDict = new Dictionary<BodygraphCenterType, List<BodygraphGate>>();
		var centers = new List<BodygraphCenter>();

		void AddCenter(BodygraphCenter center)
		{
			centers.Add(center);
			gatesDict.Add(center.Type, new List<BodygraphGate>());
		}
		
		var throat = new BodygraphCenter(BodygraphCenterType.Throat);
		AddCenter(throat);
		var head = new BodygraphCenter(BodygraphCenterType.Head);
		AddCenter(head);
		var root = new BodygraphCenter(BodygraphCenterType.Root);
		AddCenter(root);
		var ajna = new BodygraphCenter(BodygraphCenterType.Ajna);
		AddCenter(ajna);
		var splenic = new BodygraphCenter(BodygraphCenterType.Splenic);
		AddCenter(splenic);
		var solarPlexus = new BodygraphCenter(BodygraphCenterType.SolarPlexus);
		AddCenter(solarPlexus);
		var heart = new BodygraphCenter(BodygraphCenterType.Heart);
		AddCenter(heart);
		var sacral = new BodygraphCenter(BodygraphCenterType.Sacral);
		AddCenter(sacral);
		var g = new BodygraphCenter(BodygraphCenterType.G);
		AddCenter(g);

		Centers = new BodygraphCentersCollection<BodygraphCenter>(centers);

		#endregion
	
		#region Gates

		var gates = new List<BodygraphGate>(64);
		void AddGate(Hexagram hexagram, BodygraphCenter center)
		{
			var gate = new BodygraphGate(hexagram, center);
			gates.Add(gate);
			gatesDict[center.Type].Add(gate);
		}

		AddGate(1, g);
		AddGate(2, g);
		AddGate(3, sacral);
		AddGate(4, ajna);
		AddGate(5, sacral);
		AddGate(6, solarPlexus);
		AddGate(7, g);
		AddGate(8, throat);
		AddGate(9, sacral);
		AddGate(10, g);
		AddGate(11, ajna);
		AddGate(12, throat);
		AddGate(13, g);
		AddGate(14, sacral);
		AddGate(15, g);
		AddGate(16, throat);
		AddGate(17, ajna);
		AddGate(18, splenic);
		AddGate(19, root);
		AddGate(20, throat);
		AddGate(21, heart);
		AddGate(22, solarPlexus);
		AddGate(23, throat);
		AddGate(24, ajna);
		AddGate(25, g);
		AddGate(26, heart);
		AddGate(27, sacral);
		AddGate(28, splenic);
		AddGate(29, sacral);
		AddGate(30, solarPlexus);
		AddGate(31, throat);
		AddGate(32, splenic);
		AddGate(33, throat);
		AddGate(34, sacral);
		AddGate(35, throat);
		AddGate(36, solarPlexus);
		AddGate(37, solarPlexus);
		AddGate(38, root);
		AddGate(39, root);
		AddGate(40, heart);
		AddGate(41, root);
		AddGate(42, sacral);
		AddGate(43, ajna);
		AddGate(44, splenic);
		AddGate(45, throat);
		AddGate(46, g);
		AddGate(47, ajna);
		AddGate(48, splenic);
		AddGate(49, solarPlexus);
		AddGate(50, splenic);
		AddGate(51, heart);
		AddGate(52, root);
		AddGate(53, root);
		AddGate(54, root);
		AddGate(55, solarPlexus);
		AddGate(56, throat);
		AddGate(57, splenic);
		AddGate(58, root);
		AddGate(59, sacral);
		AddGate(60, root);
		AddGate(61, head);
		AddGate(62, throat);
		AddGate(63, head);
		AddGate(64, head);

		Gates = new BodygraphGatesCollection(gates);

		foreach (var (type, list) in gatesDict)
		{
			var center = Centers[type] ?? throw new Exception($"Expected center of type {type}");
			center.Gates = list.Cast<IBodygraphGate>().ToArray();
		}

		#endregion

		#region Channels
		
		var channels = new List<BodygraphChannel>();
		void AddChannel(Hexagram from, Hexagram to, BodygraphCircut counterType)
			=> channels.Add(new BodygraphChannel(Gates[from], Gates[to], counterType));
		
		AddChannel(10, 20, BodygraphCircut.Integration);
		AddChannel(34, 20, BodygraphCircut.Integration);
		AddChannel(34, 57, BodygraphCircut.Integration);
		AddChannel(57, 10, BodygraphCircut.Integration);
		
		AddChannel(61, 24, BodygraphCircut.Knowing);
		AddChannel(43, 23, BodygraphCircut.Knowing);
		AddChannel(38, 28, BodygraphCircut.Knowing);
		AddChannel(57, 20, BodygraphCircut.Knowing);
		AddChannel(1, 8, BodygraphCircut.Knowing);
		AddChannel(39, 55, BodygraphCircut.Knowing);
		AddChannel(22, 12, BodygraphCircut.Knowing);
		AddChannel(60, 3, BodygraphCircut.Knowing);
		AddChannel(14, 2, BodygraphCircut.Knowing);
		
		AddChannel(34, 10, BodygraphCircut.Centering);
		AddChannel(51, 25, BodygraphCircut.Centering);
		
		AddChannel(63, 4, BodygraphCircut.Understanding);
		AddChannel(17, 62, BodygraphCircut.Understanding);
		AddChannel(58, 18, BodygraphCircut.Understanding);
		AddChannel(48, 16, BodygraphCircut.Understanding);
		AddChannel(52, 9, BodygraphCircut.Understanding);
		AddChannel(5, 15, BodygraphCircut.Understanding);
		AddChannel(7, 31, BodygraphCircut.Understanding);
		
		AddChannel(64, 47, BodygraphCircut.Sensing);
		AddChannel(11, 56, BodygraphCircut.Sensing);
		AddChannel(41, 30, BodygraphCircut.Sensing);
		AddChannel(36, 35, BodygraphCircut.Sensing);
		AddChannel(53, 42, BodygraphCircut.Sensing);
		AddChannel(29, 46, BodygraphCircut.Sensing);
		AddChannel(13, 33, BodygraphCircut.Sensing);
		
		AddChannel(59, 6, BodygraphCircut.Defence);
		AddChannel(27, 50, BodygraphCircut.Defence);
		
		AddChannel(54, 32, BodygraphCircut.Ego);
		AddChannel(44, 26, BodygraphCircut.Ego);
		AddChannel(19, 49, BodygraphCircut.Ego);
		AddChannel(37, 40, BodygraphCircut.Ego);
		AddChannel(21, 45, BodygraphCircut.Ego);

		Channels = new BodygraphChannelsCollection(channels);

		#endregion
	}

}