using System.Xml.Serialization;

namespace OpenHumanDesign.Visualization.Schema;

public class CenterPosition
{
	[XmlAttribute]
	public BodygraphCenterType? RelativeTo;

	[XmlAttribute]
	public int X;
	
	[XmlAttribute]
	public int Y;
}
