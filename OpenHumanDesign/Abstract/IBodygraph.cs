namespace OpenHumanDesign.Abstract;

public interface IBodygraph
{
	IReadOnlyList<IBodygraphGate> Gates { get; }
	IReadOnlyCollection<IBodygraphChannel> Channels { get; }
	IReadOnlyCollection<IBodygraphCenter> Centers { get; }
}
