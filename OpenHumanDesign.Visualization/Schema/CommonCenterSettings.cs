using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace OpenHumanDesign.Visualization.Schema;

public class CommonSettings
{
	[XmlAttribute]
	public decimal Scale { get; set; } = 1m;

	[XmlElement]
	public string? CommonCenterCss { get; set; } = @"stroke: black;
	filter: drop-shadow(0px 3px 3px rgba(0, 0, 0, 0.5));
";

	[XmlElement]
	public string? AdditionalCss { get; set; } = @".channel {
	stroke: white;
	stroke-width: 4px;
}

.channel-contour {
	stroke: black;
	stroke-width: 6px;
	filter: drop-shadow(0px 3px 3px rgba(0, 0, 0, 0.5));
}

.gate-text {
	fill: black;
	font: 9px Helvetica;
	text-anchor: middle;
	alignment-baseline: middle;
}";

	[SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
	public override int GetHashCode()
	{
		var hash = new HashCode();
		hash.Add(Scale);
		hash.Add(AdditionalCss);
		hash.Add(CommonCenterCss);
		return hash.ToHashCode();
	}
}
