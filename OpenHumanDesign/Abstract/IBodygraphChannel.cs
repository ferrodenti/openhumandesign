namespace OpenHumanDesign.Abstract;

public interface IBodygraphChannel
{
	IBodygraphGate Gate1 { get; }
	IBodygraphGate Gate2 { get; }
}
