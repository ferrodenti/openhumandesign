using System.Collections;
using System.Collections.ObjectModel;
using OpenHumanDesign.Abstract;
using OpenHumanDesign.Visualization.Schema;

namespace OpenHumanDesign.Visualization.Svg;

public class BodygraphSvgRenerer
{
	readonly BodygraphSchema _schema;
	readonly ReadOnlyDictionary<BodygraphCenterType, CenterShape> _centerSchemas;

	public BodygraphSvgRenerer(BodygraphSchema schema)
	{
		_schema = schema;

		static IEnumerable<(BodygraphCenterType, CenterSchema)> EnumerateCenters(BodygraphSchema schema)
		{
			yield return (BodygraphCenterType.Head, schema.Head);
			yield return (BodygraphCenterType.Ajna, schema.Ajna);
			yield return (BodygraphCenterType.Throat, schema.Throat);
			yield return (BodygraphCenterType.G, schema.G);
			yield return (BodygraphCenterType.Sacral, schema.Sacral);
			yield return (BodygraphCenterType.Root, schema.Root);
			yield return (BodygraphCenterType.Heart, schema.Heart);
			yield return (BodygraphCenterType.Splenic, schema.Splenic);
			yield return (BodygraphCenterType.SolarPlexus, schema.SolarPlexus);
		}

		var centers = new Dictionary<BodygraphCenterType, CenterShape>();
		var position = new Point();
		
		foreach (var (type, centerSchema) in EnumerateCenters(schema))
		{
			position = type switch
			{
				BodygraphCenterType.Heart => centers[BodygraphCenterType.G].Position + centerSchema.Position,
				BodygraphCenterType.Splenic => centers[BodygraphCenterType.Sacral].Position + centerSchema.Position,
				BodygraphCenterType.SolarPlexus => centers[BodygraphCenterType.Sacral].Position + centerSchema.Position,
				_ => position + centerSchema.Position
			};
			centers.Add(type, new CenterShape(centerSchema, position));
		}

		_centerSchemas = new ReadOnlyDictionary<BodygraphCenterType, CenterShape>(centers);
	}

	public string Render(IBodygraph bodygraph)
	{
		var builder = new SeparatedStringBuilder("\n");

		foreach (var shape in _centerSchemas.Values!)
			shape.Render(builder);

		return builder;
	}
}
