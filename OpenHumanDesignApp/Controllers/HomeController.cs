﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using OpenHumanDesign.Visualization;
using OpenHumanDesign.Visualization.Abstract;
using OpenHumanDesign.Visualization.Schema;
using OpenHumanDesign.Visualization.Svg;

namespace OpenHumanDesign.Controllers
{
	public class HomeController : Controller
	{
		readonly BodygraphSchema _schema;

		public HomeController(BodygraphSchema bodygraphSchema)
		{
			_schema = bodygraphSchema;
		}

		public IActionResult Index()
		{
			var bodygraph = new Bodygraph();
			var renderer = new BodygraphSvgRenerer(_schema);
			var svg = renderer.Render(bodygraph);
			return View(new HtmlString(svg));
		}
	}
}