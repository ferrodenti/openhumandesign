using IChing;
using JetBrains.Annotations;
using OpenHumanDesign.Visualization.Abstract;
using OpenHumanDesign.Visualization.Schema;

namespace OpenHumanDesign.Visualization;

[PublicAPI]
public class BodygraphView : IBodygraphView
{
	public IBodygraphViewsFactory Factory { get; }
	public Bodygraph Bodygraph { get; }

	Lazy<BodygraphCentersCollection<IBodygraphCenterView>> _centers;
	public BodygraphCentersCollection<IBodygraphCenterView> Centers => _centers.Value;

	Lazy<IBodygraphChannelView[]> _channels;
	public IBodygraphChannelView[] Channels => _channels.Value;
	
	Lazy<OneBasedArray<IBodygraphGateView>> _gates;
	public OneBasedArray<IBodygraphGateView> Gates => _gates.Value;
	
	public string Name => Bodygraph.Name;

	public BodygraphView(Bodygraph bodygraph, BodygraphSchema schema)
		: this(bodygraph, schema, BodygraphViewsFactory.Default)
	{
	}

	public BodygraphView(Bodygraph bodygraph, BodygraphSchema schema, IBodygraphViewsFactory factory)
	{
		Factory = factory;
		Bodygraph = bodygraph;

		//_centers = new Lazy<BodygraphCentersCollection<IBodygraphCenterView>>(() => Factory.CreateCenters(Bodygraph.Centers));
		//_gates = new Lazy<OneBasedArray<IBodygraphGateView>>(() => Factory.CreateGates(Bodygraph.Gates, Centers));
		//_channels = new Lazy<IBodygraphChannelView[]>(() => Factory.DecorateChannels(Bodygraph.Channels, Gates));
	}

	public string RenderSvg()
		=> RenderSvg(false);

	public string RenderSvg(bool svgElement)
	{
		var builder = new SeparatedStringBuilder("\n");

		foreach (var channel in Channels)
			builder.Append(channel.RenderSvg());
		
		foreach (var center in Centers)
			builder.Append(center.RenderSvg());
		
		foreach (var gate in Gates)
			builder.Append(gate.RenderSvg());

		return svgElement
			? $"<svg width='100%' height='300px' xmlns='http://www.w3.org/2000/svg'>{builder}</svg>"
			: builder.ToString();
	}
}