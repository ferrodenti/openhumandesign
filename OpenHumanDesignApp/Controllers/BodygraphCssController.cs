using Microsoft.AspNetCore.Mvc;
using OpenHumanDesign.Visualization.Schema;

namespace OpenHumanDesign.Controllers;

public class BodygraphCssController : Controller
{
	readonly BodygraphSchema _bodygraphSchema;
		
	public BodygraphCssController(BodygraphSchema bodygraphSchema)
		=> _bodygraphSchema = bodygraphSchema;
		
	[Route("bodygraph.css")]
	public IActionResult Index(int v = 0)
	{
		Utils.Nop(v);
			
		var builder = new SeparatedStringBuilder("\n");

		foreach (var center in _bodygraphSchema.Centers)
		{
			builder.Append($".{center.Type.ToString().ToLower()}-center {{");
			builder.Append(_bodygraphSchema.CommonSettings.CommonCenterCss);
			builder.Append($"	fill: {center.Color};");
			builder.Append("}\n");
		}

		builder.Append(_bodygraphSchema.CommonSettings.AdditionalCss);

		return Content(builder, "text/css");
	}
}
