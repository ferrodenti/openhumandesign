using System.Globalization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace OpenHumanDesign.Visualization;

public readonly struct Size : IXmlSerializable
{
	public readonly decimal Width;
	public readonly decimal Height;

	public decimal LenSq => Width * Width + Height * Height;
	public decimal Len => (decimal)Math.Sqrt((double)LenSq);
	public bool IsEmpty => Width == 0 && Height == 0;
	public bool IsSome => Width != 0 || Height != 0;

	public Size(decimal x, decimal y)
	{
		Width = x;
		Height = y;
	}

	public static Size Parse(string str)
	{
		str = str.Trim().TrimStart('(').TrimEnd(')').Trim();
		var arr = str.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
		if (arr.Length != 2)
			throw new Exception($"Expected \"(X,Y)\" value, got: {str}");

		var width = decimal.Parse(arr[0].Trim());
		var height = decimal.Parse(arr[1].Trim());

		return new Size(width, height);
	}

	public static bool TryParse(string str, out Size result)
	{
		str = str.Trim().TrimStart('(').TrimEnd(')').Trim();
		var arr = str.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
		if (arr.Length == 2 &&
		    decimal.TryParse(arr[0].Trim(), out var width) &&
		    decimal.TryParse(arr[1].Trim(), out var height))
		{
			result = new Size(width, height);
			return true;
		}

		result = new Size();
		return false;
	}

	public override string ToString()
		=> $"({Width}, {Height})";

	public override int GetHashCode()
		=> HashCode.Combine(Width, Height);

	public override bool Equals(object obj)
		=> obj is Point b && b.X == Width && b.Y == Height;

	public static Size operator *(Size a, decimal b)
		=> new(a.Width * b, a.Height * b);

	public static Size operator /(Size a, decimal b)
		=> new(a.Width / b, a.Height / b);

	public static Size operator +(Size a, Size b)
		=> new(a.Width + b.Width, a.Height + b.Height);

	public static Size operator -(Size a, Size b)
		=> new(a.Width - b.Width, a.Height - b.Height);

	XmlSchema IXmlSerializable.GetSchema()
		=> throw new NotSupportedException();

	static bool ReadAttributeValue(XmlReader reader, string attributeName, out string value)
	{
		if (reader.MoveToAttribute(attributeName) && reader.ReadAttributeValue() && reader.HasValue)
		{
			value = reader.Value;
			return true;
		}

		value = null!;
		return false;
	}

	unsafe void IXmlSerializable.ReadXml(XmlReader reader)
	{
		var x = 0m;
		var y = 0m;

		if (ReadAttributeValue(reader, "Width", out var str))
			x = decimal.Parse(str, CultureInfo.InvariantCulture);

		if (ReadAttributeValue(reader, "Height", out str))
			y = decimal.Parse(str, CultureInfo.InvariantCulture);

		if (x != 0 || y != 0)
			fixed (Point* t = &this)
				*t = new Point(x, y);
	}

	void IXmlSerializable.WriteXml(XmlWriter writer)
	{
		writer.WriteAttributeString("Width", Width.ToString(CultureInfo.InvariantCulture));
		writer.WriteAttributeString("Height", Height.ToString(CultureInfo.InvariantCulture));
	}
}
