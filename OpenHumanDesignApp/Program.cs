using OpenHumanDesign.Visualization;
using OpenHumanDesign.Visualization.Abstract;
using OpenHumanDesign.Visualization.Schema;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddScoped(_ => BodygraphSchema.Load("default.bodygraph.schema"));
builder.Services.AddSingleton<IBodygraphViewsFactory>(BodygraphViewsFactory.Default);
//var schema = new BodygraphSchema();
//schema.Save("default.bodygraph.schema");

var app = builder.Build();

// Configure the HTTP request pipeline.
// if (!app.Environment.IsDevelopment())
// {
// 	app.UseExceptionHandler("/Home/Error");
// 	// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
// 	app.UseHsts();
// }
//
//app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseRouting();
app.UseAuthorization();
app.MapControllerRoute(
	name: "default",
	pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
