﻿namespace IChing;

[Serializable]
public readonly struct Hexagram : INGram<Hexagram>
{
	public char Symbol { get; }
	public int Number { get; }
	public int BinaryValue { get; }

	public OneBasedArray<bool> Lines { get; }

	readonly Lazy<Hexagram> _partnerLazy;
	public Hexagram Partner => _partnerLazy.Value;

	public readonly Trigram UpperTrigram;
	public readonly Trigram LowerTrigram;
	
	internal Hexagram(Hexagrams hexagrams, int number, char symbol, string bits)
	{
		Number = number;
		Symbol = symbol;

		(Lines, var binaryValue, var partnerBits) = hexagrams.InitNGram(bits);
		BinaryValue = binaryValue;
		LowerTrigram = Trigrams.Instance.GetByBinaryValue(binaryValue >> 3);
		UpperTrigram = Trigrams.Instance.GetByBinaryValue(binaryValue);
		
		_partnerLazy = new Lazy<Hexagram>(() => hexagrams.GetByBinaryValue(partnerBits));
		hexagrams.RegisterNGram(this);
	}

	public override string ToString()
		=> $"{Symbol}({Number})";

	public override int GetHashCode()
		=> 101 + Number;

	public override bool Equals(object obj)
		=> obj is Hexagram h && h.Number == Number;
	
	public static Hexagram Get(int number)
		=> Hexagrams.Instance.GetByNumber(number);

	public static Hexagram GetByBinaryValue(int value)
		=> Hexagrams.Instance.GetByBinaryValue(value); 
	
	public static implicit operator int(Hexagram hexagram)
		=> hexagram.Number;

	public static implicit operator Hexagram(int number)
		=> Hexagrams.Instance.GetByNumber(number);

	public static Hexagram Parse(string value)
		=> Hexagrams.Instance.Parse(value);
	
	public static bool TryParse(string value, out Hexagram result)
		=> Hexagrams.Instance.TryParse(value, out result);

	public static Hexagram? TryParse(string value)
		=> Hexagrams.Instance.TryParse(value);
}
