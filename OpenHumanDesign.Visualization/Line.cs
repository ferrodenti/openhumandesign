namespace OpenHumanDesign.Visualization;

public readonly struct Line
{
	public readonly decimal A;
	public readonly decimal B;
	public readonly decimal C;
	
	public Line(Point p, Point q)
	{
		var a = p.Y - q.Y;
		var b = q.X - p.X;
		var c = - a * p.X - b * p.Y;

		var z = (decimal)Math.Sqrt((double)(a * a + b * b));

		if (Math.Abs(z) > VectorUtils.Eps)
		{
			a /= z;
			b /= z;
			c /= z;
		}

		A = a;
		B = b;
		C = c;
	}
	
	
	public Line(decimal a, decimal b, decimal c)
	{
		A = a;
		B = b;
		C = c;
	}

	public override string ToString()
	{
		string Sign(decimal v)
			=> v >= 0 ? $"+ {v}" : $"- {-v}";

		var bld = new SeparatedStringBuilder(" ");
		
		if (A != 0)
			bld.Append($"{A}*x");

		if (B != 0)
			bld.Append($"{Sign(B)}*y");

		if (C != 0)
			bld.Append($"{Sign(C)}");

		bld.Append("= 0");

		return bld.ToString();
	}
}
