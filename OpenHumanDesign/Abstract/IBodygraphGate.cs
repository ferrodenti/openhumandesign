using IChing;

namespace OpenHumanDesign.Abstract;

public interface IBodygraphGate
{
	Hexagram Hexagram { get; }
}