namespace OpenHumanDesign.Visualization;

public readonly struct LineSection
{
	public readonly Point Point1;
	public readonly Point Point2;

	public LineSection(Point point1, Point point2)
	{
		Point1 = point1;
		Point2 = point2;
	}

	public Point Devide(decimal percent)
		=> Point1 * (1m - percent) + Point2 * percent;

}
