﻿using JetBrains.Annotations;

namespace OpenHumanDesign.Visualization;

[PublicAPI]
public class BodygraphRules
{
	public decimal Scale { get; set; } = 1;

	public decimal CenterWidth { get; set; } = 45;
	public decimal CenterHeight { get; set; } = 45;
	public decimal CentersDistance { get; set; } = 20;
	public decimal CenterRadius { get; set; } = 7;

	public decimal SideCentersOffset { get; set; } = 60;

	public decimal ChannelDistance { get; set; } = 5;
	public decimal ChannelWidth { get; set; } = 5;
	
	public bool DrawInnerEdges { get; set; }
}