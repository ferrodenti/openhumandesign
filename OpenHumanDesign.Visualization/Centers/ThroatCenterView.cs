using OpenHumanDesign.Abstract;

namespace OpenHumanDesign.Visualization.Centers;

public class ThroatCenterView : BodygraphCenterView
{
	public ThroatCenterView(IBodygraphCenter center, BodygraphRules rules, Point position)
		: base(center, rules, "Throat", position)
	{
	}

	protected override IEnumerable<Point> CreateShape()
	{
		yield return new Point(Position.X + HalfWidth, Position.Y);
		yield return new Point(Position.X + HalfWidth, Position.Y + Height);
		yield return new Point(Position.X - HalfWidth, Position.Y + Height);
		yield return new Point(Position.X - HalfWidth, Position.Y);
	}
}