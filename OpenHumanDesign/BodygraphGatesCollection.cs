using System.Collections;
using IChing;
using OpenHumanDesign.Abstract;

namespace OpenHumanDesign;

public class BodygraphGatesCollection : IReadOnlyList<IBodygraphGate>
{
	readonly OneBasedArray<IBodygraphGate> _inner;

	public BodygraphGatesCollection(IEnumerable<IBodygraphGate> gates)
		=> _inner = new OneBasedArray<IBodygraphGate>(gates);

	public IEnumerator<IBodygraphGate> GetEnumerator()
		=> _inner.GetEnumerator();

	IEnumerator<IBodygraphGate> IEnumerable<IBodygraphGate>.GetEnumerator()
		=> _inner.Cast<IBodygraphGate>().GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	public int Count => _inner.Count;

	public IBodygraphGate this[int index] => _inner[index];
}
