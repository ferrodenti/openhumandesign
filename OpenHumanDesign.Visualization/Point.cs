using System.Globalization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace OpenHumanDesign.Visualization;

public readonly struct Point: IXmlSerializable
{
	public readonly decimal X;
	public readonly decimal Y;

	public decimal LenSq => X * X + Y * Y;
	public decimal Len => (decimal)Math.Sqrt((double)LenSq);
	public bool IsEmpty => X == 0 && Y == 0;
	public bool IsSome => X != 0 || Y != 0;

	public Point(decimal x, decimal y)
	{
		X = x;
		Y = y;
	}

	public static Point Parse(string str)
	{
		str = str.Trim().TrimStart('(').TrimEnd(')').Trim();
		var arr = str.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
		if (arr.Length != 2)
			throw new Exception($"Expected \"(X,Y)\" value, got: {str}");

		var x = decimal.Parse(arr[0].Trim());
		var y = decimal.Parse(arr[1].Trim());

		return new Point(x, y);
	}

	public static bool TryParse(string str, out Point result)
	{
		str = str.Trim().TrimStart('(').TrimEnd(')').Trim();
		var arr = str.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
		if (arr.Length == 2 && 
		    decimal.TryParse(arr[0].Trim(), out var x) && 
		    decimal.TryParse(arr[1].Trim(), out var y))
		{
			result = new Point(x, y);
			return true;
		}

		result = new Point();
		return false;
	}
	
	public Point Norm()
		=> this / Len;

	public override string ToString()
		=> $"({X}, {Y})";
	
	public override int GetHashCode()
		=> HashCode.Combine(X, Y);

	public override bool Equals(object obj)
		=> obj is Point b && b.X == X && b.Y == Y;
	
	public static Point operator*(Point a, decimal b)
		=> new(a.X * b, a.Y * b);
	
	public static Point operator/(Point a, decimal b)
		=> new(a.X / b, a.Y / b);

	public static Point operator +(Point a, Point b)
		=> new(a.X + b.X, a.Y + b.Y);
	
	public static Point operator -(Point a, Point b)
		=> new(a.X - b.X, a.Y - b.Y);

	public static bool operator <(Point a, Point b)
		=> a.X < b.X - VectorUtils.Eps || Math.Abs(a.X - b.X) < VectorUtils.Eps && a.Y < b.Y - VectorUtils.Eps;
	
	public static bool operator >(Point a, Point b)
		=> (a.X > b.X - VectorUtils.Eps || Math.Abs(a.X - b.X) < VectorUtils.Eps)  && a.Y < b.Y - VectorUtils.Eps;


	XmlSchema IXmlSerializable.GetSchema()
		=> throw new NotSupportedException();

	static bool ReadAttributeValue(XmlReader reader, string attributeName, out string value)
	{
		if (reader.MoveToAttribute(attributeName) && reader.ReadAttributeValue() && reader.HasValue)
		{
			value = reader.Value;
			return true;
		}

		value = null!;
		return false;
	}

	unsafe void IXmlSerializable.ReadXml(XmlReader reader)
	{
		var x = 0m;
		var y = 0m;

		if (ReadAttributeValue(reader, "X", out var str))
			x = decimal.Parse(str, CultureInfo.InvariantCulture);

		if (ReadAttributeValue(reader, "Y", out str))
			y = decimal.Parse(str, CultureInfo.InvariantCulture);

		if (x != 0 || y != 0)
			fixed (Point* t = &this)
				*t = new Point(x, y);
	}

	void IXmlSerializable.WriteXml(XmlWriter writer) 
	{
		writer.WriteAttributeString("X", X.ToString(CultureInfo.InvariantCulture));
		writer.WriteAttributeString("X", Y.ToString(CultureInfo.InvariantCulture));
	}
}