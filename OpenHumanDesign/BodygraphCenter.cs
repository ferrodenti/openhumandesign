using OpenHumanDesign.Abstract;

namespace OpenHumanDesign;

public class BodygraphCenter : IBodygraphCenter
{
	public BodygraphCenterType Type { get; }

	public IBodygraphGate[] Gates { get; internal set; } 
	
	public BodygraphCenter(BodygraphCenterType type)
	{
		Type = type;
		Gates = Array.Empty<IBodygraphGate>();
	}
}
