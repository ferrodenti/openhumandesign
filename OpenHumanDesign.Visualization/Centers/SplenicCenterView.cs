using OpenHumanDesign.Abstract;

namespace OpenHumanDesign.Visualization.Centers;

public class SplenicCenterView : BodygraphCenterView
{
	public SplenicCenterView(IBodygraphCenter center, BodygraphRules rules, Point position)
		: base(center, rules, "splenic", position)
	{
	}

	protected override IEnumerable<Point> CreateShape()
	{
		var scale = (decimal)Math.Sqrt(2);
		yield return Position;
		yield return new Point(Position.X - Width * scale, Position.Y + HalfHeight * scale);
		yield return new Point(Position.X - Width * scale, Position.Y - HalfHeight * scale);
	}
}
