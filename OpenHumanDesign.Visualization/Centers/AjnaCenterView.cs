﻿using OpenHumanDesign.Abstract;

namespace OpenHumanDesign.Visualization.Centers;

public class AjnaCenterView : BodygraphCenterView
{
	public AjnaCenterView(IBodygraphCenter center, BodygraphRules rules, Point position)
		: base(center, rules, "ajna", position)
	{
	}

	protected override IEnumerable<Point> CreateShape()
	{
		const decimal scale = 1.1m;
		yield return new Point(Position.X + HalfWidth * scale, Position.Y);
		yield return new Point(Position.X, Position.Y + Height * scale);
		yield return new Point(Position.X - HalfWidth * scale, Position.Y);
	}
}