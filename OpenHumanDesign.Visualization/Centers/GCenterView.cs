using OpenHumanDesign.Abstract;

namespace OpenHumanDesign.Visualization.Centers;

public class GCenterView : BodygraphCenterView
{
	public GCenterView(IBodygraphCenter center, BodygraphRules rules, Point position)
		: base(center, rules, "G", position)
	{
	}

	protected override IEnumerable<Point> CreateShape()
	{
		var scale = (decimal)Math.Sqrt(2);
		yield return Position;
		yield return new Point(Position.X + HalfWidth * scale, Position.Y + HalfHeight * scale);
		yield return new Point(Position.X, Position.Y + Height * scale);
		yield return new Point(Position.X - HalfWidth * scale, Position.Y + HalfHeight * scale);
	}
}