﻿using OpenHumanDesign.Abstract;
using OpenHumanDesign.Visualization.Abstract;

namespace OpenHumanDesign.Visualization.Centers;

public abstract class BodygraphCenterView : IBodygraphCenterView
{
	public IBodygraphCenter Center { get; }
	public BodygraphRules Rules { get; }
	public string Name { get; }
	public Point Position { get; }
	public decimal Width { get; }
	public decimal Height { get; }

	decimal? _halfWidth;
	public decimal HalfWidth => _halfWidth ?? (_halfWidth = Width / 2m).Value;
	
	decimal? _halfHeight;
	public decimal HalfHeight => _halfHeight ?? (_halfHeight = Height / 2m).Value;

	public string ClassName { get; }

	readonly Lazy<Point[]> _shape;
	public Point[] Shape => _shape.Value;

	readonly Lazy<string> _svg;
	public string Svg => _svg.Value;

	readonly Lazy<LineSection[]> _innerEdges;
	public LineSection[] InnerEdges => _innerEdges.Value;

	public BodygraphCenterType Type => Center.Type;

	public IBodygraphGate[] Gates => throw new NotSupportedException();

	protected BodygraphCenterView(IBodygraphCenter center, BodygraphRules rules, string name, Point position)
	{
		Center = center;
		Rules = rules;
		Name = name;
		Position = position;
		Width = Rules.CenterWidth;
		Height = Rules.CenterHeight;

		ClassName = $"{Name.ToLower()}-center";

		_shape = new Lazy<Point[]>(() => CreateShape().ToArray());
		_innerEdges = new Lazy<LineSection[]>(() =>
		{
			var result = new List<LineSection>();

			var newPoints = new List<Point>();
			var last = Shape.Length - 1;
			for (var i = -1; i < last; ++i)
			{
				var v1 = Shape[i >= 0 ? i : last];
				var v2 = Shape[(i + 1) % Shape.Length];
				var v3 = Shape[(i + 2) % Shape.Length];

				var v21 = (v2 - v1).Norm();
				var d21 = new Point(-v21.Y, v21.X) * Rules.CenterRadius;
				var v1N1 = v1 + d21;
				var v2N1 = v2 + d21;

				var v23 = (v2 - v3).Norm();
				var d23 = new Point(v23.Y, -v23.X) * Rules.CenterRadius;
				var v2N2 = v2 + d23;
				var v3N2 = v3 + d23;

				var pt = VectorUtils.Intersect(v1N1, v2N1, v2N2, v3N2) ?? new Point(0, 0);
				newPoints.Add(pt);
			}

			for (var i = 0; i < newPoints.Count; ++i)
				result.Add(new LineSection(newPoints[i], newPoints[(i + 1) % newPoints.Count]));

			return result.ToArray();
		});
		_svg = new Lazy<string>(() => ShapeToArcPolygon(Shape, Rules.CenterRadius));
	}

	protected abstract IEnumerable<Point> CreateShape();

	static Point Offsets(Point a, Point b, decimal scale)
	{
		var dab = b - a;
		var r = dab * scale;
					
		var d = dab.Len;
		if (d != 0)
			r /= d;

		return r;
	}
	
	static string ShapeToArcPolygon(IList<Point> points, decimal radius)
	{
		var bld = new SeparatedStringBuilder("\n");
		var move = false;
		void Arc(Point a, Point b, Point c)
		{
			var r = Offsets(a, b, radius);

			if (!move)
			{
				move = true;
				var ar = a + r;
				bld.Append($"M {ar.X} {ar.Y}");
			}

			r = b - r;
			bld.Append($"L {r.X} {r.Y}");

			r = Offsets(b, c, radius) + b;
			bld.Append($"Q {b.X} {b.Y} {r.X} {r.Y}");
		}

		for (var i = 0; i < points.Count; ++i)
		{
			var a = (i + 0) % points.Count;
			var b = (i + 1) % points.Count;
			var c = (i + 2) % points.Count;

			Arc(points[a], points[b], points[c]);
		}

		return bld.ToString();
	}

	public string RenderSvg()
	{
		var bld = new SeparatedStringBuilder("\n");

		bld.Append($"<path class=\"{ClassName}\" d=\"{Svg}\"/>");

		if (Rules.DrawInnerEdges)
			foreach (var section in InnerEdges) 
				bld.Append($"<line x1=\"{section.Point1.X}\" y1=\"{section.Point1.Y}\" x2=\"{section.Point2.X}\" y2=\"{section.Point2.Y}\" class=\"line1\" />");
			
		return bld;
	}
}