using IChing;
using JetBrains.Annotations;
using OpenHumanDesign.Abstract;

namespace OpenHumanDesign.Visualization.Abstract;

[PublicAPI]
public interface IBodygraphViewsFactory
{
	BodygraphRules Rules { get; }

	IBodygraphCenterView CreateCenterView(IBodygraphCenter center);
	IBodygraphGateView CreateGateView(IBodygraphGate gate);
	IBodygraphChannelView CreateChannelView(IBodygraphChannel channel);

	//BodygraphCentersCollection<IBodygraphCenterView> CreateCenters(BodygraphCentersCollection centers);
	OneBasedArray<IBodygraphGateView> CreateGates(IEnumerable<BodygraphGate> gates, BodygraphCentersCollection<IBodygraphCenterView> centers);
	
	IBodygraphChannelView[] DecorateChannels(IEnumerable<BodygraphChannel> channels, OneBasedArray<IBodygraphGateView> gates);
}