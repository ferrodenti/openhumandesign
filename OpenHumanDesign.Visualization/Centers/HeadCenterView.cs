﻿using OpenHumanDesign.Abstract;

namespace OpenHumanDesign.Visualization.Centers;

public class HeadCenterView : BodygraphCenterView
{
	public HeadCenterView(IBodygraphCenter center, BodygraphRules rules, Point position)
		: base(center, rules, "head", position)
	{
	}

	protected override IEnumerable<Point> CreateShape()
	{
		const decimal scale = 1.1m;
		yield return Position;
		yield return new Point(Position.X + HalfWidth * scale, Position.Y + Height * scale);
		yield return new Point(Position.X - HalfWidth * scale, Position.Y + Height * scale);
	}
}