using IChing;
using JetBrains.Annotations;
using OpenHumanDesign.Abstract;
using OpenHumanDesign.Visualization.Abstract;
using OpenHumanDesign.Visualization.Centers;

namespace OpenHumanDesign.Visualization;

[PublicAPI]
public class BodygraphViewsFactory : IBodygraphViewsFactory
{
	public static BodygraphViewsFactory Default { get; } = new();
	
	public BodygraphRules Rules { get; set; } = new();
	
	public IBodygraphCenterView CreateCenterView(IBodygraphCenter center)
	{
		throw new NotImplementedException();
	}

	public IBodygraphGateView CreateGateView(IBodygraphGate gate)
	{
		throw new NotImplementedException();
	}

	public IBodygraphChannelView CreateChannelView(IBodygraphChannel channel)
	{
		throw new NotImplementedException();
	}


	public OneBasedArray<IBodygraphGateView> CreateGates(IEnumerable<BodygraphGate> gates, BodygraphCentersCollection<IBodygraphCenterView> centers)
	{
		throw new NotImplementedException();
	}

	public BodygraphCentersCollection<BodygraphCenterView> DecorateCenters(BodygraphCentersCollection<IBodygraphCenter> centers)
	{
		var position = new Point();
		var head = new HeadCenterView(centers.Get(BodygraphCenterType.Head), Rules, position);
		position += new Point(0, head.Height + Rules.CentersDistance);

		var ajna = new AjnaCenterView(centers.Get(BodygraphCenterType.Ajna), Rules, position);
		position += new Point(0, ajna.Height + Rules.CentersDistance);
		
		var throat = new ThroatCenterView(centers.Get(BodygraphCenterType.Throat), Rules, position);
		position += new Point(0, throat.Height + Rules.CentersDistance);
		
		var g = new GCenterView(centers.Get(BodygraphCenterType.G), Rules, position);
		position += new Point(0, g.Height /2m + Rules.CentersDistance );

		var position2 = position + new Point(g.Width * (2/3m) + Rules.CentersDistance, 0);
		var heart = new HeartCenterView(centers.Get(BodygraphCenterType.Heart), Rules, position2);
		position += new Point(0, heart.Height + Rules.CentersDistance * 1.5m);

		position2 = position + new Point(Rules.SideCentersOffset, 0);
		var solapPlexus = new SolarPlexusCenterView(centers.Get(BodygraphCenterType.SolarPlexus), Rules, position2);
		position2 = position + new Point(-Rules.SideCentersOffset, 0);
		var splenic = new SplenicCenterView(centers.Get(BodygraphCenterType.Splenic), Rules, position2);
		//position += new Point(0, Rules.CentersDistance);
		
		var sacral = new SacralCenterView(centers.Get(BodygraphCenterType.Sacral), Rules, position);
		position += new Point(0, sacral.Height + Rules.CentersDistance);
		
		var root = new RootCenterView(centers.Get(BodygraphCenterType.Root), Rules, position);

		return new BodygraphCentersCollection<BodygraphCenterView>(new BodygraphCenterView[] { head, ajna, throat, g, heart, solapPlexus, splenic, sacral, root });
	}
	public OneBasedArray<IBodygraphGateView> DecorateGates(IEnumerable<BodygraphGate> gates, BodygraphCentersCollection<BodygraphCenterView> centers)
	{
		var result = new OneBasedArray<IBodygraphGateView>(gates.Select(g => (IBodygraphGateView)new GateView(centers.Get(g.Center.Type), Rules, g)));

		// Head - Ajna:
		
		result[64].BindToCenterPosition(1, 95);
		result[47].BindToCenterPosition(2, 5);
		
		result[61].BindToCenterPosition(1, 50);
		result[24].BindToCenterPosition(2, 50);
		
		result[63].BindToCenterPosition(1, 5);
		result[4].BindToCenterPosition(2, 95);
		
		var leftX = result[47].CenterPoint?.X ?? 0;
		var rightX = result[4].CenterPoint?.X ?? 0;
		
		// Ajna - Throat:
		
		result[17].BindToCenterX(1, leftX).TextShift = new Point(3, 10);
		result[62].BindToCenterX(3, leftX);
		
		result[43].BindToCenterPosition(1, 0);
		result[23].BindToCenterPosition(3, 50);

		result[11].BindToCenterX(0, rightX).TextShift = new Point(-3, 10);
		result[56].BindToCenterX(3, rightX);
		
		// Throat - G:
		
		result[31].BindToCenterX(1, leftX);
		result[7].BindToCenterX(3, leftX);
		
		result[8].BindToCenterPosition(1, 50);
		result[1].BindToCenterPosition(3, 100);
		
		result[33].BindToCenterX(1, rightX);
		result[13].BindToCenterX(0, rightX);
		
		
		// G - Sacral:
		
		result[15].BindToCenterX(2, leftX);
		result[5].BindToCenterX(3, leftX);
		
		result[2].BindToCenterPosition(2, 0);
		result[14].BindToCenterPosition(3, 50);
		
		result[46].BindToCenterX(1, rightX);
		result[29].BindToCenterX(3, rightX);
		
		// Sacral - Root:
		
		result[42].BindToCenterX(1, leftX);
		result[53].BindToCenterX(3, leftX);
		
		result[3].BindToCenterPosition(1, 50);
		result[60].BindToCenterPosition(3, 50);
		
		result[9].BindToCenterX(1, rightX);
		result[52].BindToCenterX(3, rightX);
		
		
		// Heart:
		
		result[25].BindToCenterPosition(0, 100);
		result[51].BindToCenterPosition(2, 50);

		result[26].BindToCenterPosition(2, 0).TextShift = new Point(-3, 2.5m);
		result[44].BindToCenterPosition(2, 66);
		
		result[21].BindToCenterPosition(0, 10).TextShift = new Point(-2, -2);
		result[45].BindToCenterPosition(0, 80);
		
		result[40].BindToCenterPosition(1, 30).TextShift = new Point(0, 2);
		result[37].BindToCenterPosition(0, 33);
		
		// SolarPlexus:
		
		result[22].BindToCenterPosition(0, 66);
		result[12].BindToCenterPosition(0, 50);
		
		result[36].BindToCenterPosition(0, 100);
		result[35].BindToCenterPosition(0, 20);
		
		result[6].BindToCenterPosition(0, 0);
		result[59].BindToCenterPosition(0, 60);
		
		
		//TODO: initialize gate positions
		
		return result;
	}

	public IBodygraphChannelView[] DecorateChannels(IEnumerable<BodygraphChannel> channels, OneBasedArray<IBodygraphGateView> gates)
		=> channels.Select(ch => (IBodygraphChannelView)new ChannelView(gates[ch.Gate1.Hexagram], gates[ch.Gate2.Hexagram])).ToArray();
}
