using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace OpenHumanDesign.Visualization.Schema;

public class CenterSchema
{
	[XmlIgnore]
	public BodygraphCenterType Type { get; internal set; }
	
	[XmlAttribute]
	public ShapeType Shape { get; set; }
	
	[XmlAttribute]
	public string? Color { get; set; }
	
	[XmlElement]
	public Size Size { get; set; }
	
	[XmlElement]
	public Point Position { get; set; }
	
	[XmlElement]
	public decimal Radius { get; set; }

	[XmlElement("Gate")]
	public GateSchema[]? Gates { get; set; }

	[SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
	public override int GetHashCode()
	{
		var hash = new HashCode();
		
		hash.Add(Shape);
		hash.Add(Color);
		hash.Add(Size);
		hash.Add(Position);

		if(Gates != null)
			foreach (var gate in Gates)
				hash.Add(gate);

		return hash.ToHashCode();
	}
}