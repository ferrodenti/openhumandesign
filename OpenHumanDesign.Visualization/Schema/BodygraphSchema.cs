using System.Diagnostics.CodeAnalysis;
using System.Xml;
using System.Xml.Serialization;

namespace OpenHumanDesign.Visualization.Schema;

public class BodygraphSchema
{
	[XmlElement]
	public CenterSchema Head { get; set; } = new()
	{
		Color = "yellow",
		Gates = new [] {
			new GateSchema(64, 1, .95m),
			new GateSchema(61, 1, .5m),
			new GateSchema(63, 1, .05m),
		}
	};

	[XmlElement]
	public CenterSchema Ajna { get; set; } = new()
	{
		Color = "mediumseagreen",
		Gates = new [] {
			new GateSchema(47, 2, 64),
			new GateSchema(24, 2, 61),
			new GateSchema(4, 2, 63),
			
			new GateSchema(17, 1, 64) { TextShift = new Point(1,2)},
			new GateSchema(43, 1, 61),
			new GateSchema(11, 0, 63),
		}
	};

	[XmlElement] public CenterSchema Throat { get; set; } = new()
	{
		Color = "sandybrown"
	};

	[XmlElement]
	public CenterSchema G { get; set; } = new()
	{
		Color = "yellow"
	};

	[XmlElement]
	public CenterSchema Heart { get; set; } = new()
	{
		Color = "indianred"
	};

	[XmlElement]
	public CenterSchema Sacral { get; set; } = new()
	{
		Color = "indianred"
	};

	[XmlElement]
	public CenterSchema Root { get; set; } = new()
	{
		Color = "sandybrown"
	};

	[XmlElement]
	public CenterSchema Splenic { get; set; } = new()
	{
		Color = "sandybrown"
	};

	[XmlElement]
	public CenterSchema SolarPlexus { get; set; } = new()
	{
		Color = "sandybrown"
	};
	
	[XmlElement]
	public CommonSettings CommonSettings { get; set; } = new();
	
	[XmlIgnore] string? _filename;

	[SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
	public override int GetHashCode()
	{
		var hash = new HashCode();

		hash.Add(_filename);
		hash.Add(CommonSettings);

		hash.Add(Head);
		hash.Add(Ajna);
		hash.Add(Throat);
		hash.Add(G);
		hash.Add(Sacral);
		hash.Add(Root);
		hash.Add(Splenic);
		hash.Add(SolarPlexus);
		
		return hash.ToHashCode();
	}

	public void Save(string filename)
	{
		_filename = filename;
		using var stream = File.OpenWrite(filename);
		stream.SetLength(0);
		WriteXml(stream);
	}
	
	public void WriteXml(Stream stream)
	{
		using var wr = XmlWriter.Create(stream, new XmlWriterSettings()
		{
			Indent = true,
			IndentChars = "\t",
			OmitXmlDeclaration = true,
			NamespaceHandling = NamespaceHandling.OmitDuplicates,
		});
		
		var ns = new XmlSerializerNamespaces();
		ns.Add("", "");

		_xmlSerializer.Serialize(wr, this, ns);
	}

	static readonly XmlSerializer _xmlSerializer = new(typeof(BodygraphSchema));

	public static BodygraphSchema Load(string filename)
	{
		using var stream = File.OpenRead(filename);
		var result = Load(stream);
		result._filename = filename;
		return result;
	}
	
	public static BodygraphSchema Load(Stream stream)
		=> (BodygraphSchema)_xmlSerializer.Deserialize(stream);
}
