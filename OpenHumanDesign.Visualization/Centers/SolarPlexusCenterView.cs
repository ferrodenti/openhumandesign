using OpenHumanDesign.Abstract;

namespace OpenHumanDesign.Visualization.Centers;

public class SolarPlexusCenterView : BodygraphCenterView
{
	public SolarPlexusCenterView(IBodygraphCenter center, BodygraphRules rules, Point position)
		: base(center, rules, "Solar-Plexus", position)
	{
	}

	protected override IEnumerable<Point> CreateShape()
	{
		var scale = (decimal)Math.Sqrt(2);
		yield return Position;
		yield return new Point(Position.X + Width * scale, Position.Y - HalfHeight * scale);
		yield return new Point(Position.X + Width * scale, Position.Y + HalfHeight * scale);
	}
}