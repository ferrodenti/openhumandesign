using System.Collections;
using OpenHumanDesign.Abstract;

namespace OpenHumanDesign;

public class BodygraphCentersCollection<TCenter> : 
	IReadOnlyCollection<TCenter> 
	where TCenter : IBodygraphCenter
{
	readonly TCenter? _throat;
	readonly TCenter? _head;
	readonly TCenter? _root;
	readonly TCenter? _ajna;
	readonly TCenter? _splenic;
	readonly TCenter? _solarPlexus;
	readonly TCenter? _heart;
	readonly TCenter? _sacral;
	readonly TCenter? _g;

	public BodygraphCentersCollection(IEnumerable<TCenter> centers)
	{
		static void Add(ref TCenter? target, TCenter center)
		{
			if (target != null)
				throw new ArgumentException($"An item with the same type has already been added. Type: {target.Type}");

			target = center;
		}
		
		foreach (var center in centers)
		{
			switch (center.Type)
			{
			case BodygraphCenterType.Throat: Add(ref _throat, center); break;
			case BodygraphCenterType.Head: Add(ref _head, center); break;
			case BodygraphCenterType.Root: Add(ref _root, center); break;
			case BodygraphCenterType.Ajna: Add(ref _ajna, center); break;
			case BodygraphCenterType.Splenic: Add(ref _splenic, center); break;
			case BodygraphCenterType.SolarPlexus: Add(ref _solarPlexus, center); break;
			case BodygraphCenterType.Heart: Add(ref _heart, center); break;
			case BodygraphCenterType.Sacral: Add(ref _sacral, center); break;
			case BodygraphCenterType.G: Add(ref _g, center); break;
			default:
				throw new ArgumentOutOfRangeException(nameof(centers), centers, $"Invalid center type: {center.Type}");
			}
		}
	}
	IEnumerable<TCenter> Enumerate()
	{
		if (_throat != null) yield return _throat;
		if (_head != null) yield return _head;
		if (_root != null) yield return _root;
		if (_ajna != null) yield return _ajna;
		if (_splenic != null) yield return _splenic;
		if (_solarPlexus != null) yield return _solarPlexus;
		if (_heart != null) yield return _heart;
		if (_sacral != null) yield return _sacral;
		if (_g != null) yield return _g;
	}

	public IEnumerator<TCenter> GetEnumerator()
		=> Enumerate().GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	public bool Contains(TCenter item)
	{
		var current = this[item.Type];
		return Equals(current, item);
	}

	int? _count;
	public int Count => _count ?? (_count = Enumerate().Count()).Value;

	public bool IsReadOnly => false;

	public int IndexOf(TCenter item)
	{
		var result = 0;
		foreach (var center in Enumerate())
		{
			if (Equals(center, item))
				return result;
			
			++result;
		}

		return -1;
	}
	
	public TCenter? this[int index]
	{
#pragma warning disable CS8766
		get => Enumerate().Skip(index).FirstOrDefault();
#pragma warning restore CS8766
	}

	public TCenter Get(BodygraphCenterType type)
		=> this[type] ?? throw new Exception($"Could not find a center of type {type}");

	public TCenter? this[BodygraphCenterType type] => type switch
	{
		BodygraphCenterType.Throat => _throat,
		BodygraphCenterType.Head => _head,
		BodygraphCenterType.Root => _root,
		BodygraphCenterType.Ajna => _ajna,
		BodygraphCenterType.Splenic => _splenic,
		BodygraphCenterType.SolarPlexus => _solarPlexus,
		BodygraphCenterType.Heart => _heart,
		BodygraphCenterType.Sacral => _sacral,
		BodygraphCenterType.G => _g,
		_ => throw new ArgumentOutOfRangeException(nameof(type), type, "Invalid center type")
	};
}
