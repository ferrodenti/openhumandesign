using IChing;
using OpenHumanDesign.Visualization.Abstract;

namespace OpenHumanDesign.Visualization;

public class GateView : IBodygraphGateView
{
	public Point? CenterPoint { get; set; }
	public BodygraphGate Gate {get;}
	public IBodygraphCenterView Center { get; } 
	public BodygraphRules Rules { get; }

	public Point? TextShift { get; set; }
	
	public GateView(IBodygraphCenterView centerView, BodygraphRules bodygraphRules, BodygraphGate gate)
	{
		Gate = gate;
		Center = centerView;
		Rules = bodygraphRules;
	}
	
	public IBodygraphGateView BindToCenterPosition(int edgeNo, decimal position)
	{
		var edge = Center.InnerEdges[edgeNo];
		CenterPoint = edge.Devide(position * 0.01m);
		return this;
	}

	public IBodygraphGateView BindToCenterX(int edgeNo, decimal xPos)
	{
		var edge = Center.InnerEdges[edgeNo];
		var yPos = edge.Point1.Y;
		var dx = edge.Point2.X - edge.Point1.X;
		if (dx != 0)
			yPos += (xPos - edge.Point1.X) * (edge.Point2.Y - edge.Point1.Y) / dx;
		
		CenterPoint = new Point(xPos, yPos);
		return this;
	}
	
	public string RenderSvg()
	{
		if (!CenterPoint.HasValue)
			return "";

		var pos = CenterPoint.Value;

		if (TextShift.HasValue)
			pos += TextShift.Value;

		return $"<text x='{pos.X}' y='{pos.Y}' class='gate-text'>{Gate.Hexagram.Number}</text>";
	}

	public Hexagram Hexagram { get; }
}