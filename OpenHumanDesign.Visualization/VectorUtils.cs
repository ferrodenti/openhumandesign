namespace OpenHumanDesign.Visualization;

public static class VectorUtils
{
	public const decimal Eps = (decimal)1E-9;

	public static Point? Intersect(Point a, Point b, Point c, Point d)
	{
		if (!Intersect1d(a.X, b.X, c.X, d.X) ||
		    !Intersect1d(a.Y, b.Y, c.Y, d.Y))
			return null;

		var m = new Line(a, b);
		var n = new Line(c, d);
		var zn = Det(m.A, m.B, n.A, n.B);

		if (Math.Abs(zn) < Eps)
			return null;

		var result = new Point(-Det(m.C, m.B, n.C, n.B) / zn, -Det(m.A, m.C, n.A, n.C) / zn);
		if (Betw(a.X, b.X, result.X)
		    && Betw(a.Y, b.Y, result.Y)
		    && Betw(c.X, d.X, result.X)
		    && Betw(c.Y, d.Y, result.Y))
			return result;
		
		return null;
	}

	static decimal Det(decimal a, decimal b, decimal c, decimal d)
		=> a * d - b * c;

	static bool Betw(decimal l, decimal r, decimal x)
		=> Math.Min(l, r) <= x + Eps && x <= Math.Max(l, r) + Eps;

	static bool Intersect1d (decimal a, decimal b, decimal c, decimal d)
	{
		if (a > b) (a, b) = (b, a);
		if (c > d) (c, d) = (d, c);
		return Math.Max (a, c) <= Math.Min (b, d) + Eps;
	}
}
