using OpenHumanDesign.Abstract;

namespace OpenHumanDesign.Visualization.Centers;

public class HeartCenterView : BodygraphCenterView
{
	public HeartCenterView(IBodygraphCenter center, BodygraphRules rules, Point position)
		: base(center, rules, "Heart", position)
	{
	}

	protected override IEnumerable<Point> CreateShape()
	{
		var sqrt = (decimal)Math.Sqrt(2);
		yield return Position;
		yield return new Point(Position.X + sqrt * HalfWidth, Position.Y + sqrt * HalfHeight);
		yield return new Point(Position.X - sqrt * HalfWidth, Position.Y + sqrt * HalfHeight);
	}
}
