namespace OpenHumanDesign.Visualization.Schema;

public enum ShapeType
{
	Rect,
	Diamond,
	TriangleUp,
	TriangleDown,
	TriangleLeft,
	TriangleRight,
	Octagon
}
